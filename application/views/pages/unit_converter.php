<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$convertUnits = 0;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
   $weight = $_POST['weight'];
   $fromConvertionUnit = $_POST['fromConvertionUnit'];
   $toConvertionUnit = $_POST['toConvertionUnit'];

   if($fromConvertionUnit == $toConvertionUnit){
    $convertUnits = $weight;
    }
   else if($fromConvertionUnit == 'lbs' && $toConvertionUnit == 'kg') {
	$convertUnits = ($weight) * 0.45;
	}
   else if($fromConvertionUnit == 'kg' && $toConvertionUnit == 'lbs') {
	$convertUnits = ($weight) * 2.2046;
    }

}
?>

<!doctype html>
<html lang="en">
   <head>
       <meta charset="UTF-8">
       <h1>Pretvornik Teže (Lbs/Kg)</h1>
   </head>
   <body>
       <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
           <div>
			   <br>
               <label for="weight">Teža:</label><br/>
               <input type="number" name="weight" id="weight" value="<?php echo (!empty($weight)) ? $weight : '' ?>">
           </div>
		<br>
		<br>
           <div>
             <label for="fromConvertionUnit">Pretvorite iz Enote:</label><br/>
              <select name="fromConvertionUnit" id="fromConvertionUnit">
                <option value="lbs">lbs(pounds)</option>
                <option value="kg">kilogrami</option>
            </select>
		   </div>
	   <br>
	   <br>
          <div>
             <label for="toConvertionUnit">V Enote:</label><br/>
             <select name="toConvertionUnit" id="toConvertionUnit">
               <option value="lbs">lbs(pounds)</option>
               <option value="kg">kilogrami</option>
            </select>
           </div>
		<br>
		<br>
           <div>
       Teža <b> <?php  echo (!empty($weight)) ? $weight : '--' ?></b> enot <b><?php echo  (!empty($fromConvertionUnit)) ? $fromConvertionUnit : '--' ?></b> = <b><?php echo (!empty($convertUnits)) ? $convertUnits : '--' ?><b/> <b><?php echo  (!empty($toConvertionUnit)) ? $toConvertionUnit : '--' ?></b>
    <br>
	<label for="converted_value"></label><br/> <input type="number" value="<?php echo (!empty($convertUnits)) ? $convertUnits : '0' ?>">
    </div>
    <div>
	<br>
	<input type="submit" value="Pretvorite">
	<br>
	<br>
       </div>
     </form>
   </body>
</html>