<?php
class Plants_model extends CI_Model{

	public function __construct(){
		$this->load->database();
		}

	public function get_plants(){
		$query = $this->db->get('plants');
		return $query->result_array();
		}

	public function get_plants_where($slug){
		$query = $this->db->get_where('plants', array('slug' => $slug));
		return $query->row_array();
		}

	public function set_plants(){

		$slug = url_title($this->input->post('title'), 'dash', TRUE);

		$data = array(
			'title' => $this->input->post('title'),
			'slug' => $slug,
			'text' => $this->input->post('text'));

		return $this->db->insert('plants', $data);
		
			
	}
}