<?php
class Plants extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('plants_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
		// Load form helper library
		$this->load->helper('form');
		// Load form validation library
		$this->load->library('form_validation');
		$this->load->helper('url', 'form');


	}

	public function create(){

		if(isset($this->session->userdata['logged_in'])){
			$this->load->helper('form');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('text', 'Text', 'required');

			$data['title'] = "Dodajte Ikebano...";

			if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
       		$this->load->view('plants/create');
        	$this->load->view('templates/footer');
			}
			else{
			$this->plants_model->set_plants();
			$this->load->view('plants/success');
			}
		}
		
		else{
			$data['message_display'] = 'Signin to view admin page!';
			$this->load->view('templates/header');
			$this->load->view('user_authentication/login_form', $data);
			$this->load->view('templates/footer');
		}
	}


	public function view($slug)
	{
		
		$data['plants_item'] = $this->plants_model->get_plants_where($slug);
	
		$data['title'] = "Plants Item";
	

		$this->load->view('templates/header', $data);
        $this->load->view('plants/view', $data);
        $this->load->view('templates/footer', $data);

	}


    public function index(){
		
		$data['plants'] = $this->plants_model->get_plants();
		$data['title'] = "Arhiv Ikeban";
		
		$this->load->view('files/upload_form');
		$this->load->view('templates/header', $data);
        $this->load->view('plants/index', $data);
        $this->load->view('templates/footer', $data);
		}
		
	public function store(){
        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 2000;
        $config['max_width'] = 1500;
        $config['max_height'] = 1500;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile_image')) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('files/upload_form', $error);
        } else {
            $data = array('image_metadata' => $this->upload->data());

            $this->load->view('files/upload_result', $data);
        }
    }

	public function signup(){
		echo "<h3>NEED TO IMPLEMENT SIGNUP</h3>";
	}

	public function Login(){
		echo "<h3>NEED TO IMPLEMENT LOGIN</h3>";
	}


}